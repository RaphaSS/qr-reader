export interface RuleSet {
	name: string;
	description: string;
}

export type RuleSetList = { [qrCode: string]: RuleSet };