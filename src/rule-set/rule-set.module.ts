import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicStorageModule } from '@ionic/storage';

import { RuleSetProvider } from './rule-set.provider';

import { RuleSetPage } from './rule-set.page';
import { IonicModule } from 'ionic-angular/module';

@NgModule({
	imports: [
		CommonModule,
		IonicModule.forRoot(RuleSetPage),
		IonicStorageModule.forRoot()
	],
	entryComponents: [RuleSetPage],
	declarations: [RuleSetPage],
	providers: [RuleSetProvider]
})
export class RuleSetModule { }