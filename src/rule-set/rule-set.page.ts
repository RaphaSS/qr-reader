import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
	selector: 'page-rule-set',
	templateUrl: 'rule-set.page.html'
})
export class RuleSetPage {

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams
	) {
		navParams.get('qr');
	}

}
