import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { RuleSet, RuleSetList } from './rule-set';

const RULES: string = 'rules';

@Injectable()
export class RuleSetProvider {

	constructor(public storage: Storage) { }

	findAll(): Promise<RuleSetList> {
		return this.storage.get(RULES);
	}

	save(qrCode: string, ruleSet: RuleSet): Promise<any> {
		let rules: RuleSetList;
		this.findAll().then(list => rules = list || {});

		rules[qrCode] = ruleSet;

		return this.storage.set(RULES, rules);
	}

}
