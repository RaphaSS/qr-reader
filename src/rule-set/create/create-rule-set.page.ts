import { Component, ViewChild } from "@angular/core";
import { NgForm } from '@angular/forms';
import { ViewController } from 'ionic-angular/navigation/view-controller';

import { RuleSetProvider } from './../rule-set.provider';
import { NavParams } from "ionic-angular/navigation/nav-params";

@Component({
	selector: 'create-rule-set',
	templateUrl: 'create-rule-set.page.html'
})
export class CreateRuleSetPage {

	@ViewChild('form') form: NgForm;

	constructor(
		public viewCtrl: ViewController,
		public params: NavParams,
		public ruleSetProvider: RuleSetProvider
	) { }

	save() {
		this.ruleSetProvider.save(this.params.data.code, this.form.value)
			.then(ruleSet => this.viewCtrl.dismiss({ ruleSet }));
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

}