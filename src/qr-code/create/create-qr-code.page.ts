import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ViewController } from 'ionic-angular';

import { QrCodeProvider } from '../qr-code.provider';

@Component({
	selector: 'create-qr-code',
	templateUrl: 'create-qr-code.page.html'
})
export class CreateQrCodePage {

	@ViewChild('form') form: NgForm;

	constructor(
		public viewCtrl: ViewController,
		public qrCodeProvider: QrCodeProvider
	) { }

	save() {
		this.qrCodeProvider.generate(this.form.value.code)
			.then(() => this.viewCtrl.dismiss({ code: this.form.value.code }));
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

}