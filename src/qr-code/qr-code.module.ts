import { QrCodeProvider } from './qr-code.provider';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { IonicModule } from 'ionic-angular/module';

import { CreateQrCodePage } from "./create/create-qr-code.page";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule.forRoot(CreateQrCodePage)
	],
	entryComponents: [CreateQrCodePage],
	declarations: [CreateQrCodePage],
	providers: [QrCodeProvider]
})
export class QrCodeModule { }