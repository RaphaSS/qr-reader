import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { QrCodeModule } from './../qr-code/qr-code.module';
import { RuleSetModule } from './../rule-set/rule-set.module';

import { App } from './app.component';
import { HomePage } from './../home/home.page';

@NgModule({
	declarations: [
		App,
		HomePage
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(App),
		RuleSetModule,
		QrCodeModule
	],
	bootstrap: [IonicApp],
	entryComponents: [
		App,
		HomePage
	],
	providers: [
		StatusBar,
		SplashScreen,
		{ provide: ErrorHandler, useClass: IonicErrorHandler }
	]
})
export class AppModule { }
