import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { RuleSetProvider } from './../rule-set/rule-set.provider';
import { RuleSet, RuleSetList } from '../rule-set/rule-set';
import { RuleSetPage } from './../rule-set/rule-set.page';
import { CreateRuleSetPage } from './../rule-set/create/create-rule-set.page';

import { CreateQrCodePage } from './../qr-code/create/create-qr-code.page';

@Component({
	selector: 'page-home',
	templateUrl: 'home.page.html'
})
export class HomePage {

	ruleSets: RuleSetList = {};

	constructor(
		public navCtrl: NavController,
		public modalCtrl: ModalController,
		public ruleSetProvider: RuleSetProvider
	) {
		this.load();
	}

	load() {
		this.ruleSetProvider.findAll().then(ruleSets => this.ruleSets = ruleSets || {});
	}

	createQrCode() {
		this.modalCtrl.create(CreateQrCodePage).present()
			.then((data: { code: string }) => {
				if (data) {
					this.modalCtrl.create(CreateRuleSetPage, data).present()
						.then(() => this.load());
				}
			});
	}

	rulesArray(): [string, RuleSet][] {
		const arr: [string, RuleSet][] = [];
		for (let k of Object.keys(this.ruleSets)) {
			arr.push([k, this.ruleSets[k]]);
		}
		return arr;
	}

	open(qr) {
		this.navCtrl.push(RuleSetPage, { qr });
	}

}
